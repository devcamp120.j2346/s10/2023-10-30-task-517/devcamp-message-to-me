import "bootstrap/dist/css/bootstrap.min.css";

import Header from "./components/header/Header";
import Body from "./components/body/Body";

function App() {
  return (
    <div className="container text-center">
      <Header />

      <Body />
    </div>
  );
}

export default App;
